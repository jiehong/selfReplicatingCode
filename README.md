# Self Replicating Code

This repository provides a base for a Coding Dojo I prepared exploring
[self replicating codes, aka Quines](https://en.wikipedia.org/wiki/Quine_(computing)).

There is 1 repository per language containing a starter with a unit test
to indicate when the purpose has been reached.

## Mission

Your mission is to write a programme that takes no input from the command line.
When executed, it must print its entire source code to the standard output.
