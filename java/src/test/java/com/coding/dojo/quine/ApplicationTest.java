package com.coding.dojo.quine;

import org.junit.*;

import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;

public class ApplicationTest {
  private final ByteArrayOutputStream standardOutput = new ByteArrayOutputStream();
  private final ByteArrayOutputStream standardError = new ByteArrayOutputStream();

  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(standardOutput));
    System.setErr(new PrintStream(standardError));
  }

  @After
  public void cleanUpStreams() {
    System.setOut(null);
    System.setErr(null);
  }

  @Test
  public void checkOutputMatchesCode() {
    Application.main(Collections.emptyList().toArray(new String[0]));
    Assert.assertEquals(0, standardError.size());
    Assert.assertEquals(readApplicationFile(), standardOutput.toString());
  }

  private String readApplicationFile() {
    final File file = new File("./src/main/java/com/coding/dojo/quine/Application.java");
    try {
      return String.join("\n", Files.readAllLines(file.toPath(), Charset.forName("UTF-8")));
    } catch (final IOException e) {
      return "Unable to read source file";
    }
  }
}
